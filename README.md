# GNN_DOVE

> https://github.com/kiharalab/GNN_DOVE

## Installation

Please clone the repository via SSH or HTTPs.

### Docker

For launching experiments, please

1. Install Docker following the documentation https://docs.docker.com/get-docker/.
1. For Linux, please execute the post-installation
   https://docs.docker.com/engine/install/linux-postinstall/.
1. Enter the root of the repository `GNN_DOVE/`.
1. Build docker image

   ```bash
   make setup-local  # with GPU
   make setup-no-gpu # without GPU
   ```

1. Enter the docker container
   ```bash
   make bash
   ```
1. Verify the installation
   ```bash
   python main.py --mode=0 -F example/input/correct.pdb --fold=1
   ```

### Dev

For local code development

1. Install the conda environment
   ```bash
   conda env create -f environment.yml        # with GPU
   conda env create -f environment_no_gpu.yml # without GPU
   ```
2. Install pre-commit hooks
   ```bash
   pre-commit install
   ```
