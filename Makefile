IMAGE_NAME = gnn
CONTAINER_NAME = gnn


ifeq ($(DOCKER_VERSION), 19.03)
	DOCKER_GPU_COMMAND = --gpus all
else
	DOCKER_GPU_COMMAND = --runtime nvidia
endif

build:
	docker build -t $(IMAGE_NAME) --build-arg host_gid=$$(id -g) --build-arg host_uid=$$(id -u) -f Dockerfile.local .

run-local:
	docker run -it -d -e MACHINE_ID=`hostname` --name $(CONTAINER_NAME) $(DOCKER_GPU_COMMAND) -v ${PWD}:/home/app/GNN_DOVE --restart always $(IMAGE_NAME):latest

run-no-gpu:
	docker run -it -d -e MACHINE_ID=`hostname` --name $(CONTAINER_NAME)  -v ${PWD}:/home/app/GNN_DOVE --restart always $(IMAGE_NAME):latest

bash:
	docker exec -it $(CONTAINER_NAME) sh -c "/bin/bash"

remove:
	docker stop $(CONTAINER_NAME)
	docker rm $(CONTAINER_NAME)

setup-local:
	make remove || true
	make build run-local

setup-no-gpu:
	make remove || true
	make build run-no-gpu
